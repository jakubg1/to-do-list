import { List } from "src/todo/entities/list.entity"
import { Task } from "src/todo/entities/task.entity"
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, OneToMany, JoinColumn } from "typeorm"

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column({unique: true})
    email:string

    @Column({unique: true})
    nick: string

    @Column()
    password: string

    @CreateDateColumn()
    createDate: Date

    @Column({ default: true })
    isActive: boolean

    @Column({ default: false })
    isAdmin: boolean   
    
    @OneToMany(() => List, (list) => list.user)
    @JoinColumn()
    lists: List[]

    @OneToMany(() => Task, (task) => task.user)
    @JoinColumn()
    tasks: Task[]
}