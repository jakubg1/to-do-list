import { Expose } from "class-transformer"

export class ReturnUserDto {
    @Expose()
    email: string

    @Expose()
    nick: string

    @Expose()
    createDate: Date
}