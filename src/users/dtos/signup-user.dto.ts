import { IsEmail, IsString, Length, MaxLength } from "class-validator";

export class SignupDto {
    
    @IsString()
    @IsEmail()
    email: string

    @IsString()
    @MaxLength(30)
    nick: string

    @IsString()
    @Length(8,20)
    password: string

}