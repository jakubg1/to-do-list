import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { scrypt as _scrypt } from 'crypto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

    async createUser (userData) {
        const {email, nick} = userData
        const password = userData.result
        const user = this.userRepo.create({email, nick, password})
        return this.userRepo.save(user)
    }

    findUserByEmail (email: string) {
        return this.userRepo.findOne({email})
    }

    findUserByNick (nick: string) {
        return this.userRepo.findOne({nick})
    }

    findUserById (id: string) {
        if(!id){
            return null
        }
        return this.userRepo.findOne({id})
    }

    deleteUser (id: string) {
        return this.userRepo.delete({id})
    }
}
