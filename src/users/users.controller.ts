import { Body, Controller, Get, Injectable, Post, Session, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth.guard';
import { NotAuthGuard } from 'src/guards/not-auth.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { AuthService } from './auth.service';
import { CurrentUser } from './decorators/current-user.decorator';
import { LoginDto } from './dtos/login-user.dto';
import { ReturnUserDto } from './dtos/return-user.dto';
import { SignupDto } from './dtos/signup-user.dto';
import { User } from './entities/user.entity';
import { CurrentUserInterceptor } from './interceptors/current-user.interceptor';
import { UsersService } from './users.service';

@Serialize(ReturnUserDto)
@UseInterceptors(CurrentUserInterceptor)
@Injectable()
@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService, private authService: AuthService) {}

    @UseGuards(NotAuthGuard)
    @Post('signup')
    async signup(@Body() data: SignupDto, @Session() session: any) {
        const user = await this.authService.signup(data)
        session.userID = user.id
        return user
    }

    @UseGuards(NotAuthGuard)
    @Post('login')
    async login(@Body() data: LoginDto, @Session() session: any) {
        const user = await this.authService.login(data)
        session.userID = user.id
        return user
    }

    @UseGuards(AuthGuard)
    @Get('logout')
    logout(@Session() session: any) {  
        session.userID = null
        return 'logged out'
    }


    @Get('delete')
    async delete(@Session() session: any, @CurrentUser() user: User){
        await this.usersService.deleteUser(user.id)
        session.userID = null
        return 'user deleted'
    }

}

