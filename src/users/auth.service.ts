import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { promisify } from "util";
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { UsersService } from "./users.service";

const scrypt = promisify(_scrypt);


@Injectable()
export class AuthService {
    constructor(private usersService: UsersService) {}

    async signup (userData) {
        const {email, nick, password} = userData
        const user = await this.usersService.findUserByEmail(email)
        if(user){ 
            throw new BadRequestException('Email is already registered')
        }

        const salt = randomBytes(8).toString('hex')
        const hash = (await scrypt(password, salt, 32)) as Buffer
        const result = salt + '.' + hash.toString('hex')
        const createdUser = await this.usersService.createUser({email, nick, result})

        return createdUser
        
    }

    async login (data) {
        const {email, password} = data
        const user = await this.usersService.findUserByEmail(email)
        if(!user) {
            throw new NotFoundException('user not found')
        }

        const [salt, storedHash] = user.password.split('.')

        const hash = (await scrypt(password, salt, 32)) as Buffer

        if(storedHash !== hash.toString('hex')){
            throw new BadRequestException('wrong password')
        }

        return user
    }

    
}
