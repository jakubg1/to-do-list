import { CanActivate, ExecutionContext } from "@nestjs/common"

export class NotAuthGuard implements CanActivate {
    canActivate(context: ExecutionContext){
        const req = context.switchToHttp().getRequest()
        return !Boolean(req.session.userID) 
    }
} 