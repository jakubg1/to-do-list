import { Body, Controller, Get, Param, Post, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { CurrentUser } from 'src/users/decorators/current-user.decorator';
import { User } from 'src/users/entities/user.entity';
import { CurrentUserInterceptor } from 'src/users/interceptors/current-user.interceptor';
import { CreateListDto } from './dtos/create-list.dto';
import { ReturnListDto } from './dtos/return-list.dto';
import { TodoService } from './todo.service';

@Controller('todo')
@UseGuards(AuthGuard)
// @Serialize(ReturnListDto)
@UseInterceptors(CurrentUserInterceptor)
export class TodoController {
    constructor(private todoService: TodoService) {}

    @Get()
    lists(@CurrentUser() currentUser: User) {
        return this.todoService.getLists(currentUser)
    }

    @Get(':slug')
    list(@Param('slug') slug: string, @CurrentUser() currentUser: User) {
        return this.todoService.getList(currentUser, slug)
    }

    @Post('new')
    createList(@Body() data: CreateListDto, @CurrentUser() currentUser: User) {
        return this.todoService.createList(currentUser, data)
    }

    @Get('delete/:slug')
    deleteList(@Param('slug') slug: string, @CurrentUser() currentUser: User) {
        return this.todoService.deleteList(currentUser, slug)
    }

    @Post(':slug/addTask')
    addTask(@Body('content') content: string, @Param('slug') slug: string, @CurrentUser() user: User) {
        return this.todoService.addTask(content, slug, user)
    }

    @Post(':slug/deleteTask')
    deleteTask(@Body('id') id: string, @CurrentUser() user: User){
        return this.todoService.deleteTask(user, id)
    }

}



// JWT
// Profile User