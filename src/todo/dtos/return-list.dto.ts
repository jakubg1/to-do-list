import { Expose } from 'class-transformer'
import { Task } from '../entities/task.entity'

export class ReturnListDto {
    @Expose()
    title: string

    @Expose()
    description: string

    @Expose()
    tasks: Task[]

}