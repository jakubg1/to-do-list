import {IsString, Length} from 'class-validator'

export class CreateListDto {
    @IsString()
    title: string

    @IsString()
    @Length(0, 400)
    description: string
}