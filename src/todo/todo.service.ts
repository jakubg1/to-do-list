import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm"
import { User } from 'src/users/entities/user.entity';
import { Repository } from "typeorm"
import { CreateListDto } from './dtos/create-list.dto';
import { List } from './entities/list.entity';
import { Task } from './entities/task.entity';

@Injectable()
export class TodoService {
    constructor(@InjectRepository(List) private listRepo: Repository<List>, @InjectRepository(Task) private taskRepo: Repository<Task>) {}

    getLists (user: User){
        return this.listRepo.find({user})
    }

    async getList (user: User, slug: string){
        return await this.listRepo.findOne({
            where: {
                user: user,
                slug: slug
            },
            relations: ['tasks'],
        });
      }

    async createList (user: User, data: CreateListDto) {
        const slug = data.title.replace(' ', '_').replace('/', '_').replace('\\', '_').toLowerCase()  
        const existingList = await this.listRepo.find({user, slug})
        if (existingList.length) {
            throw new BadRequestException('List with this title has already exist')
        }
        const list = this.listRepo.create({...data, slug})
        list.user = user
        return this.listRepo.save(list)
    }

    async deleteList (user: User, slug: string){
        const existingList = await this.listRepo.find({user, slug})
        if (existingList.length) {
            return this.listRepo.delete({user, slug})
        } else{
            throw new BadRequestException("List doesn't exist")
        }

    }

    async addTask (content: string, slug: string, user: User){
        const list = await this.listRepo.findOne({user, slug})
        if (!list) {
            throw new BadRequestException('List not found')
        }
        const task = await this.taskRepo.create({content, list, user})
        return await this.taskRepo.save(task)
    }

    async deleteTask (user: User, id: string) {
        const task = await this.taskRepo.findOne({user, id})

        if (task){
            return await this.taskRepo.remove(task)
        }else{
            return new BadRequestException('Task not found')
        }
    }


}

