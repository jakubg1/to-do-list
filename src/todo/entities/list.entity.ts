import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from "typeorm"
import { Length } from "class-validator"
import { User } from "src/users/entities/user.entity"
import { Task } from "./task.entity"

@Entity()
export class List {
    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column()
    @Length(1, 100)
    title: string

    @Column()
    slug: string

    @Column()
    @Length(0, 400)
    description: string

    @ManyToOne(() => User, (user) => user.lists, { onDelete: 'CASCADE'})
    @JoinColumn()
    user: User

    @OneToMany(() => Task, (task) => task.list)
    @JoinColumn()
    tasks: Task[]
}