import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from "typeorm"
import { Length } from "class-validator"
import { List } from "./list.entity"
import { User } from "src/users/entities/user.entity"

@Entity()
export class Task {
    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column()
    @Length(0, 200)
    content: string

    @ManyToOne(() => List, (list) => list.tasks, { onDelete: 'CASCADE'})
    @JoinColumn()
    list: List

    @ManyToOne(() => User, (user) => user.tasks, { onDelete: 'CASCADE'})
    @JoinColumn()
    user: User
}