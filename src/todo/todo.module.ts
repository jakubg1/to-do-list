import { Module } from '@nestjs/common';
import { TodoService } from './todo.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { TodoController } from './todo.controller';
import { List } from './entities/list.entity';
import { UsersModule } from 'src/users/users.module';
import { Task } from './entities/task.entity';

@Module({
  imports:[TypeOrmModule.forFeature([List, Task]), UsersModule],
  providers: [TodoService],
  controllers: [TodoController]
})
export class TodoModule {}
