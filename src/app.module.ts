import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { TodoModule } from './todo/todo.module';
import { List } from './todo/entities/list.entity';
import { Task } from './todo/entities/task.entity';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      entities:[User, List, Task],
      synchronize: true
    }),
    TodoModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
